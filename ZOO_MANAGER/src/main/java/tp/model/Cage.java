package tp.model;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Cage {
    /**
     * The name of this cage
     */
	@Column
    private String name;

	/**
     * The id of this cage
     */
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private UUID id_cage;

    /**
     * The visitor entrance location
     */
	@Embedded
    private Position position;

    /**
     * The maximum number of animals in this cage
     */
	@Column
    private Integer capacity;

    /**
     * The animals in this cage.
     */
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinColumn(name="id_cage")
    private Collection<Animal> residents;

    public Cage() {
    }

    public Cage(String name, Position position, Integer capacity, Collection<Animal> residents) {
        this.name = name;
        this.position = position;
        this.capacity = capacity;
        this.residents = residents;
    }

    public UUID getIdCage() {
        return id_cage;
    }

    public String getName() {
        return name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Position getPosition() {
        return position;
    }

    public Collection<Animal> getResidents() {
        return residents;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setResidents(Collection<Animal> residents) {
        this.residents = residents;
    }
}
