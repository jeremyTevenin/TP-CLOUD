package tp.rest;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

public class MyClient {

	// Path of generated files (scenario)
    public static final String GENERATED_FILES = "src/main/java/tp/scenario";

    public static final int NB_DALMATIEN = 101;

	private Service service;
    private JAXBContext jc;

    private static final QName qname = new QName("", "");
    private static final String url = "http://127.0.0.1:8080/rest-service/zoo-manager";

	/**
	 * Construct : initialize XML Parser with Java with classes :
	 * Center, Cage, Animal, Position
	 */
    public MyClient() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Cannot create JAXBContext " + je);
        }
    }

    public Source request(String urlParam, String httpMethod, Object ObjectContent) throws JAXBException {
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + urlParam);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, httpMethod);
        Source result = null;
        try {
        	result = dispatcher.invoke(new JAXBSource(jc, ObjectContent));
        } catch (Exception e) {
            Integer statusCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
            if (statusCode != 200) {
            	return new StreamSource(new StringReader("<?xml version=\"1.0\"?><root>"
            			+ "Error : " + statusCode
            			+ "</root>"));
            }
        }
        return result;
    }

	/**
	 * Méthodes permettant de faire le lien avec le serveur et permettant d'executer
	 * des requêtes
	 * @throws JAXBException
	 */

	/**
	 * Requete de type GET retournant l'ensemble des animaux du centre.
     */
    public Source get_animals() throws JAXBException {
    	return request("/animals", "GET", new Animal());
    }

    /**
	 * Requete de type POST ajoutant un animal dans le centre.
     */
    public Source add_animal(Animal animal) throws JAXBException {
    	return request("/animals", "POST", animal);
    }

    /**
	 * Requete de type DELETE supprimant l'ensemble des animaux du centre.
     */
    public Source remove_animals() throws JAXBException {
        return request("/animals", "DELETE", new Animal());
    }

    /**
	 * Requete de type PUT modifiant l'ensemble des animaux du centre.
     */
    public Source put_animals(Animal animal) throws JAXBException {
    	return request("/animals", "PUT", animal);
    }

    /**
	 * Requete de type POST ajoutant une cage au centre.
     */
    public Source add_cage(Cage cage) throws JAXBException {
    	return request("/cages", "POST", cage);
    }

    /**
	 * Requete de type DELETE supprimant l'ensemble des résidents d'une cage.
     */
    public Source remove_animals(String cageName) throws JAXBException {
        return request("/cages/" + cageName, "DELETE", new Cage());
    }

    /**
	 * Requete de type POST créant l'animal identifié par l'id.
     */
    public Source add_animal_id(Animal animal, String uuid) throws JAXBException {
    	return request("/animals/" + uuid, "POST", animal);
    }

    /**
	 * Requete de type PUT modifiant l'animal identifié par l'id.
     */
    public Source put_animal_id(Animal animal, String uuid) throws JAXBException {
    	return request("/animals/" + uuid, "PUT", animal);
    }

    /**
	 * Requete de type DELETE supprimant l'animal identifié par l'id.
     */
    public Source delete_animal_id(String uuid) throws JAXBException {
    	return request("/animals/" + uuid, "DELETE", new Animal());
    }

    /**
	 * Requete de type GET recherchant un animal par son nom.
     */
    public Source find_animal_by_name(String name) throws JAXBException {
    	return request("/find/byName/" + name, "GET", new Animal());
    }

    /**
	 * Requete de type GET recherchant un animal par position.
     */
    public Source find_animal_at_position(Position position) throws JAXBException {
    	return request("/find/at/" + position.getLatitude() + ":" + position.getLongitude(), "GET", new Animal());
    }

    /**
	 * Requete de type GET retournant l'ensemble des animaux près d'une position.
     */
    public Source find_animal_near_position(Position position) throws JAXBException {
    	return request("/find/near/" + position.getLatitude() + ":" + position.getLongitude(), "GET", new Animal());
    }

    /**
	 * Requete de type GET récupérant les informations wolfram d'un animal.
     */
    public Source find_animal_wolf(String uuid) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname,
        		HTTPBinding.HTTP_BINDING,
        		url + "/animals/" + uuid + "/wolf");

        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");

        Source result = dispatcher.invoke(new StreamSource(new StringReader("<?xml version=\"1.0\"?><root></root>")));

        return result;
    }

    public void printSource(Source s) {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.transform(s, new StreamResult(System.out));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void printSource(Source s, File f) {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.transform(s, new StreamResult(f));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String args[]) throws Exception {
        MyClient client = new MyClient();

        // Affiche l'ensemble des animaux
        Source result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario01.xml"));

        // Supprime touts les animaux
        result = client.remove_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario02.xml"));

        // Affiche l'ensemble des animaux
        result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario03.xml"));

        // Ajoute un panda à Rouen
        Cage rouen = new Cage("Rouen", new Position(49.443889, 1.10333), 10, new LinkedList<>());
        client.add_cage(rouen);
        Animal panda = new Animal("Panda", rouen.getName(), "Panda", UUID.randomUUID());
        result = client.add_animal(panda);
        client.printSource(result, new File(GENERATED_FILES + "/scenario04.xml"));

        // Ajoute un Hocco unicorne à Paris
        Cage paris = new Cage("Paris", new Position(48.856578, 2.351828), 10, new ArrayList<>());
        client.add_cage(paris);
        Animal hocco = new Animal("Hocco", paris.getName(), "Hocco unicorne", UUID.randomUUID());
        result = client.add_animal(hocco);
        client.printSource(result, new File(GENERATED_FILES + "/scenario05.xml"));

        // Afficher l'ensemble des animaux
        result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario06.xml"));

        // Modifie l'ensemble des animaux par un Lagotriche à queue jaune à Rouen
        Animal lagotriche = new Animal("Lagotriche", rouen.getName(), "Lagotriche à queue jaune", UUID.randomUUID());
        result = client.put_animals(lagotriche);
        client.printSource(result, new File(GENERATED_FILES + "/scenario07.xml"));

        // Affiche l'ensemble des animaux
        result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario08.xml"));

        // Ajoute une Océanite de Matsudaira en Somalie
        Cage somalie = new Cage("Somalie", new Position(2.333333, 48.85), 10, new ArrayList<>());
        client.add_cage(somalie);
        Animal oceanite = new Animal("Océanite", somalie.getName(), "Océanite de Matsudaira", UUID.randomUUID());
        result = client.add_animal(oceanite);
        client.printSource(result, new File(GENERATED_FILES + "/scenario09.xml"));

        // Ajoute un Ara de Spix à Rouen (Latitude : 49.443889 ; Longitude : 1.103333)
        Animal ara = new Animal("Ara", rouen.getName(), "Ara de Spix", UUID.randomUUID());
        result = client.add_animal(ara);
        client.printSource(result, new File(GENERATED_FILES + "/scenario10.xml"));

        // Ajoute un Galago de Rondo à Bihorel (Latitude : 49.455278 ; Longitude : 1.116944)
        Cage bihorel = new Cage("Bihorel", new Position(49.45278, 1.116944), 10, new ArrayList<>());
        client.add_cage(bihorel);
        Animal galago = new Animal("Galago", bihorel.getName(), "Galago de Rondo", UUID.randomUUID());
        result = client.add_animal(galago);
        client.printSource(result, new File(GENERATED_FILES + "/scenario11.xml"));

        // Ajoute une Palette des Sulu à Londres (Latitude : 51.504872 ; Longitude : ­0.07857)
        Cage londres = new Cage("Londres", new Position(51.504872, 0.07857), 10, new ArrayList<>());
        client.add_cage(londres);
        Animal palette = new Animal("Palette", londres.getName(), "Palette des Sulu", UUID.randomUUID());
        result = client.add_animal(palette);
        client.printSource(result, new File(GENERATED_FILES + "/scenario12.xml"));

        // Ajoute un Kouprey à Paris (Latitude : 48.856578 ; Longitude : 2.351828)
        Animal kouprey = new Animal("Kouprey", paris.getName(), "Kouprey", UUID.randomUUID());
        result = client.add_animal(kouprey);
        client.printSource(result, new File(GENERATED_FILES + "/scenario13.xml"));

        // Ajoute un Tuit­tuit à Paris (Latitude : 48.856578 ; Longitude : 2.351828)
        Animal tuit­tuit = new Animal("Tuit­tuit", paris.getName(), "Tuit­tuit", UUID.randomUUID());
        result = client.add_animal(tuit­tuit);
        client.printSource(result, new File(GENERATED_FILES + "/scenario14.xml"));

        // Ajoute une Saïga au Canada (Latitude : 43.2 ; Longitude : ­80.38333)
        Cage canada = new Cage("Canada", new Position(43.2, 80.38333), 10, new ArrayList<>());
        client.add_cage(canada);
        Animal saiga = new Animal("Saïga", canada.getName(), "Saïga", UUID.randomUUID());
        result = client.add_animal(saiga);
        client.printSource(result, new File(GENERATED_FILES + "/scenario15.xml"));

        // Ajoute un Inca de Bonaparte à Porto­Vecchio (Latitude : 41.5895241 ; Longitude : 9.2627)
        Cage porto­Vecchio = new Cage("Porto­Vecchio", new Position(43.2, 80.38333), 10, new ArrayList<>());
        client.add_cage(porto­Vecchio);
        Animal inca = new Animal("Inca", porto­Vecchio.getName(), "Inca de Bonaparte", UUID.randomUUID());
        result = client.add_animal(inca);
        client.printSource(result, new File(GENERATED_FILES + "/scenario16.xml"));

        // Affiche l'ensemble des animaux
        result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario17.xml"));

        // Ajoute un Râle de Zapata à Montreux (Latitude : 46.4307133; Longitude : 6.9113575)
        Cage montreux = new Cage("Montreux", new Position(46.4307133, 6.9113575), 10, new ArrayList<>());
        client.add_cage(montreux);
        Animal rale = new Animal("Râle", montreux.getName(), "Râle de Zapata", UUID.randomUUID());
        result = client.add_animal(rale);
        client.printSource(result, new File(GENERATED_FILES + "/scenario18.xml"));

        // Ajoute un Rhinocéros de Java à Villers­Bocage (Latitude : 50.0218 ; Longitude : 2.3261)
        Cage villers­Bocage = new Cage("Villers­Bocage", new Position(50.0218, 2.3261), 10, new ArrayList<>());
        client.add_cage(villers­Bocage);
        Animal rhinoceros = new Animal("Rhinocéros", villers­Bocage.getName(), "Rhinocéros de Java", UUID.randomUUID());
        result = client.add_animal(rhinoceros);
        client.printSource(result, new File(GENERATED_FILES + "/scenario19.xml"));

        // Ajoute 101 Dalmatiens dans une cage aux USA
        for (int i = 1; i <= NB_DALMATIEN; i++) {
        	Animal dalmatien = new Animal("Dalmatien" + i, "usa", "Dalmatien", UUID.randomUUID());
            result = client.add_animal(dalmatien);
        }
        client.printSource(result, new File(GENERATED_FILES + "/scenario20.xml"));

        // Affiche l'ensemble des animaux
        result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario21.xml"));

        // Supprime tous les animaux de Paris
        result = client.remove_animals(paris.getName());
        client.printSource(result, new File(GENERATED_FILES + "/scenario22.xml"));

        // Affiche l'ensemble des animaux
        result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario23.xml"));

        // Recherche le Galago de Rondo
        result = client.find_animal_by_name(galago.getName());
        client.printSource(result, new File(GENERATED_FILES + "/scenario24.xml"));

        // Supprime le Galago de Rondo
        result = client.delete_animal_id(galago.getId().toString());
        client.printSource(result, new File(GENERATED_FILES + "/scenario25.xml"));

        // Supprime à nouveau le Galago de Rondo
        result = client.delete_animal_id(galago.getId().toString());
        client.printSource(result, new File(GENERATED_FILES + "/scenario26.xml"));

        // Affiche l'ensemble des animaux
        result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario27.xml"));

        // Affiche les animaux située près de Rouen
        result = client.find_animal_near_position(new Position(rouen.getPosition().getLatitude() - 0.1,
        		rouen.getPosition().getLongitude() - 0.1));
        client.printSource(result, new File(GENERATED_FILES + "/scenario28.xml"));

        // Affiche les animaux à Rouen
        result = client.find_animal_at_position(rouen.getPosition());
        client.printSource(result, new File(GENERATED_FILES + "/scenario29.xml"));

        // Affiche les informations Wolfram Alpha du Saïga
        //result = client.find_animal_wolf(saiga.getId().toString());
        //client.printSource(result, new File(GENERATED_FILES + "/scenario30.xml"));

        // Affiche les informations Wolfram Alpha de l'Ara de Spix
        //result = client.find_animal_wolf(ara.getId().toString());
        //client.printSource(result, new File(GENERATED_FILES + "/scenario31.xml"));

        // Grapphopper non implémenté pour les scnéarios 32 et 33

        // Supprime tous les animaux
     /*   result = client.remove_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario34.xml"));
      */
        // Affiche l'ensemble des animaux
        result = client.get_animals();
        client.printSource(result, new File(GENERATED_FILES + "/scenario35.xml"));
    }
}