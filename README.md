# TP 3 & 4 d'Architectures Distribuées

Vous aurez à rendre ce projet en binôme pour le **30 Mars 2017**. 
Tout retard sera pénalisé suivant une règle de calcul exponentielle (1 point, 3 points, 6 points, etc.).

Le projet est à rendre par **email** sous la forme d'une **adresse vers un projet GIT accessible depuis le web**. 
Vous ajouterez votre encadrant pour qu'il puisse avoir accès à votre projet en vous assurant que celui-ci contienne:
1. Les **noms et prénoms** de votre binôme dans un fichier **AUTEURS** à la racine
2. L'**adresse** publique de votre service déployé en PaaS
3. Le **code** de votre service RESTFUL
4. Une documentation de votre service dans un fichier **README** à la racine
5. Un **rapport** sur les problèmes rencontrés et les solutions que vous y avez apportés et l'intégration du système de persistence

Vous pouvez également télécharger une version [PDF de l'énoncé du TP](TP-CLOUD.pdf).

L'adresse de notre service PaaS est la suivante : 

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager

Plusieurs commande y sont disponible

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/animals 				retourne tous les animaux (GET)

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/animals 				modifie tous les animaux (PUT)

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/animals 				supprime tous les animaux (DELETE)



https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/animals/{id}			retourne l'animal avec l'id passé dans l'URL (GET)

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/animals/{id}			modifie l'animal avec l'id passé dans l'URL (PUT)

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/animals/{id}			créer l'animal avec l'id passé dans l'URL (POST)

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/animals/{id}			supprime l'animal avec l'id passé dans l'URL (DELETE)



https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/find/byName/{name} 	retourne l'animal par son nom

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/find/at/{position}	retourne l'animal par position

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/find/near/{position} 	retourne d'animaux près d'une position



Nous avons décidé d'ajouter deux méthodes supplémentaires pour permettre au client de rajouter/enlever des cages
ainsi que d'enlever les résidents d'une cages

	/**
	 * Requete de type POST ajoutant une cage au centre.
     */
    add_cage(Cage cage) throws JAXBException
    
    /**
	 * Requete de type DELETE supprimant l'ensemble des résidents d'une cage.
     */
	remove_animals(String cageName) throws JAXBException
	
	
https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/cages					ajoute une cage au centre (POST)

https://tp-cloud-abjt.eu-gb.mybluemix.net/zoo-manager/cages/{name}			supprime l'ensemble des résidents d'une cage (DELETE)

Ces commandes sont implémentés dans notre service.
Nous n'avons pas réalisé la requête permettant de récupérer les informations du trajet depuis une position GPS.

Voici notre fichier manifest.yml :

applications:

- name: TP-CLOUD-ABJT

random-route: false

memory: 256M

path: target/rest-service.war

buildpack: java_buildpack


Ces paramètres permettent de donner l'adresse du lien de notre service PaaS
random-route : false permet de ne pas avoir un chemin d'accès aléatoire
