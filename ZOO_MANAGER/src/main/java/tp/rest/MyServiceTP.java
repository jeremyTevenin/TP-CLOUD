package tp.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.xml.bind.JAXBException;
import javax.xml.ws.http.HTTPException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.web.bind.annotation.RequestBody;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

@Path("/zoo-manager/")
public class MyServiceTP {

	private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");

	/* Permet de gérer la persistance des animaux */
	private Session session;

	public MyServiceTP() {
		// Configuration of the session
		Configuration configuration = new Configuration().configure();
    	configuration.addAnnotatedClass(Center.class);
    	configuration.addAnnotatedClass(Animal.class);
    	configuration.addAnnotatedClass(Position.class);
    	configuration.addAnnotatedClass(Cage.class);

	    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
	            .applySettings(configuration.getProperties()).build();

	    SessionFactory sessionFactory = configuration
	            .buildSessionFactory(serviceRegistry);

	    session = sessionFactory.openSession();

        // Fill our center with some animals
	    Animal usa1 = new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID());
	    Animal usa2 = new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID());

        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(usa1, usa2))
        );

	    Animal amazon1 = new Animal("Canine", "amazon", "Piranha", UUID.randomUUID());
	    Animal amazon2 = new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID());
	    Animal amazon3 = new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID());
	    Animal amazon4 = new Animal("De lait", "amazon", "Piranha", UUID.randomUUID());

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(amazon1, amazon2,
                		amazon3, amazon4))
        );
        center.getCages().addAll(Arrays.asList(usa, amazon));

        // Commit the change in database
        Transaction transaction = session.beginTransaction();
    	session.save(center);

    	transaction.commit();
    }

    /**
     * GET method bound to calls on /animals/{something}
     */
    @GET
    @Path("/animals/{id}/")
    @Produces("application/xml")
    public Animal getAnimal(@PathParam("id") String animal_id) throws JAXBException {
    	Animal animal = session.find(Animal.class, UUID.fromString(animal_id));

    	if (animal != null) {
        	return animal;
        } else {
        	throw new HTTPException(404);
        }
    }

    @POST
    @Path("/animals/{id}/")
    @Consumes({"text/xml", "application/xml", "application/**xml" })
    @Produces("application/xml")
    public Animal postAnimal(@PathParam("id") String animal_id, @RequestBody Animal animal) throws JAXBException {
    	animal.setId(UUID.fromString(animal_id));

    	Transaction transaction = session.beginTransaction();

    	// Query
    	Cage c = (Cage) session.createQuery("FROM Cage C WHERE C.name='" + animal.getCage() + "'").list().get(0);

    	if (c == null) {
    		throw new HTTPException(404);
    	}

    	c.getResidents().add(animal);
    	transaction.commit();

		return animal;
    }

    @PUT
    @Path("/animals/{id}/")
    @Consumes({"text/xml", "application/xml", "application/**xml" })
    @Produces("application/xml")
    public Animal putAnimal(@PathParam("id") String animal_id, @RequestBody Animal animal) throws JAXBException {
    	Animal animalBD = session.find(Animal.class, animal_id);

    	if (animalBD == null) {
        	throw new HTTPException(404);
        }

    	Transaction transaction = session.beginTransaction();
    	transaction.begin();

    	animalBD.setCage(animal.getCage());
    	animalBD.setName(animal.getName());
    	animalBD.setSpecies(animal.getSpecies());

    	transaction.commit();
    	return animalBD;
    }

    @DELETE
    @Path("/animals/{id}/")
    @Produces("application/xml")
    public Center deleteAnimal(@PathParam("id") String animal_id) throws JAXBException {
    	Animal animal = session.find(Animal.class, UUID.fromString(animal_id));

    	if (animal == null) {
    		throw new HTTPException(404);
    	}

    	List<Cage> cages = (List<Cage>) session.createQuery("FROM Cage").list();

    	Transaction transaction = session.beginTransaction();

    	cages.forEach(cage -> {
    		if (cage.getResidents().contains(animal)) {
    			Animal toRemoveAnimal = cage.getResidents()
    				.stream()
    				.filter(ani -> ani.getId().equals(animal.getId()))
    				.findFirst().orElse(null);

    			cage.getResidents().remove(toRemoveAnimal);
    	    	session.delete(toRemoveAnimal);
    		}
    	});

    	transaction.commit();

    	return (Center) session.createQuery("From Center").list().get(0);
    }


    /**
     * GET method bound to calls on /animals
     */
    @GET
    @Path("/animals/")
    @Produces("application/xml")
    public Center getAnimals(){
    	return (Center) session.createQuery("FROM Center")
    			.list()
    			.stream()
    			.findFirst()
    			.get();
    }

    /**
     * POST method bound to calls on /animals
     */
    @POST
    @Path("/animals/")
    @Consumes({"text/xml", "application/xml", "application/**xml" })
    @Produces("application/xml")
    public Animal postAnimals(@RequestBody Animal animal) throws JAXBException {
    	Transaction transaction = session.beginTransaction();

    	Cage c = (Cage) session.createQuery("FROM Cage C WHERE C.name=:cageName")
    			.setParameter("cageName", animal.getCage())
    			.list()
    			.get(0);

    	c.getResidents().add(animal);
    	transaction.commit();

		return animal;
    }

    @PUT
    @Path("/animals/")
    @Consumes({"text/xml", "application/xml", "application/**xml" })
    @Produces("application/xml")
    public Center putAnimals(@RequestBody Animal animal) {
    	List<Animal> animals = (List<Animal>) session.createQuery("From Animal").list();

    	Transaction transaction = session.beginTransaction();

    	animals.forEach(oneAnimal -> {
    		oneAnimal.setCage(animal.getCage());
    		oneAnimal.setName(animal.getName());
    		oneAnimal.setSpecies(animal.getSpecies());
    	});

    	transaction.commit();

    	return (Center) session.createQuery("From Center")
    			.list()
    			.stream()
    			.findFirst()
    			.orElse(new Center());
    }

    @DELETE
    @Path("/animals/")
    @Produces("application/xml")
    public Center deleteAnimals() {
    	List<Cage> cages = (List<Cage>) session.createQuery("FROM Cage").list();

    	Transaction transaction = session.getTransaction();

    	cages.forEach(cage -> {
    		Collection<Animal> animals = cage.getResidents();
    		cage.getResidents().removeAll(animals);
    		animals.forEach(ani -> session.delete(ani));
    	});

    	transaction.commit();

    	return (Center) session.createQuery("From Center")
    			.list()
    			.get(0);
    }

    @GET
    @Path("/find/byName/{name}/")
    @Produces("application/xml")
    public Animal searchAnimalByName(@PathParam("name") String animal_name) throws JAXBException {

		return (Animal) session.createQuery("From Animal a where a.name='" + animal_name + "'").list().get(0);
    }

    @GET
    @Path("/find/at/{position}/")
    @Produces("application/xml")
    public Cage searchAnimalByPosition(@PathParam("position") String position) throws JAXBException {
    	String[] pos = position.split(":");
		Double latitude = Double.parseDouble(pos[0]);
		Double longitude = Double.parseDouble(pos[1]);
		Cage cage = (Cage) session
				.createQuery("From Cage c where c.position.latitude=:latitude "
						+ "and c.position.longitude=:longitude")
				.setParameter("latitude", latitude)
				.setParameter("longitude", longitude)
				.list()
				.get(0);

		return cage;
    }

    @GET
    @Path("/find/near/{position}/")
    @Produces("application/xml")
    public Center searchAnimalNear(@PathParam("position") String position) throws JAXBException{
    	String[] pos = position.split(":");
		Double latitude = Double.parseDouble(pos[0]);
		Double longitude = Double.parseDouble(pos[1]);
		Center center = new Center();

		session.createQuery("From Cage c where "
				+ "c.position.latitude>:latitude-0.5 "
				+ "and c.position.latitude<:latitude+0.5 "
				+ "and c.position.longitude>:longitude-0.5 "
				+ "and c.position.longitude<:longitude+0.5 ")
			.setParameter("latitude", latitude)
			.setParameter("longitude", longitude)
			.list()
			.get(0);

		return center;
    }

    @GET
    @Path("/animals/{id}/wolf/")
    @Produces("application/xml")
    public String animalWolfram(@PathParam("id") String animal_id) throws JAXBException {
    	Animal anim = session.find(Animal.class,UUID.fromString(animal_id));
    	if (anim == null) {
    		throw new HTTPException(404);
    	}

		try {
			String species = anim.getSpecies();
			URL url = new URL("https://api.wolframalpha.com/v2/query"
					+ "?input=" + species + "&format=image,plaintext"
							+ "&output=XML&appid=KR7WRP-HAX4RG5UY9");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/xml");
			OutputStream os = conn.getOutputStream();
			os.flush();
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String result = "";
			String line ="";
			while ((line = br.readLine()) != null) {
				result = result + line;
			}

			conn.disconnect();
			return result;
		  } catch (MalformedURLException e) {
			  throw new HTTPException(404);
		  } catch (IOException e) {
			  throw new HTTPException(404);
		 }
    }

    @DELETE
    @Path("/cages/{name}/")
    @Produces("application/xml")
    public Center cageCrud(@PathParam("name") String cageName) throws JAXBException {
    	Cage cage = (Cage) session.createQuery("from Cage where cage.name=:cageName")
    			.setParameter("cageName", cageName)
    			.list()
    			.stream().findFirst()
    			.orElse(null);

    	if (cage == null) {
    		throw new HTTPException(404);
    	}

    	Center center = (Center) session.createQuery("FROM Center").list().get(0);

    	Transaction transac = session.beginTransaction();
		Cage toRemoveCage = center.getCages()
					.stream()
					.filter(c -> c.getName().equals(cageName))
					.findFirst().orElse(null);
			center.getCages().remove(toRemoveCage);
    	session.delete(toRemoveCage);

    	transac.commit();
    	return (Center) session.createQuery("From Center").list().get(0);
    }

    @POST
    @Path("/cages/")
    @Consumes({"text/xml", "application/xml", "application/**xml" })
    @Produces("application/xml")
    public Center addCage(@RequestBody Cage cage) throws JAXBException {
    	Transaction transaction = session.getTransaction();

    	Center c = (Center) session.createQuery("FROM Center").list().get(0);

    	cage.setResidents(new LinkedList<>());
    	c.getCages().add(cage);
    	transaction.commit();

		return center;
    }
}

